/* -----------------------------------------------------------------------------
@organization:
	ISEL - PG - Grupo 12

@summary:
	Class Grid with one function - main(String args[]).
	Program that draws a grid in the console.
	The program ask user for the number of columns (2..8), number of rows (2..5),
	width of the rows (1..5), and hight of the columns (1..8).

@notes:
	The program uses 4 while loops to get and validate user inputs, 3 nested for
	loops (1 for loop inside another for loop), that gives a total of 6 for
	loops, to build 3 different kinds of rows, and 1 last nested for loop, that
	gives a total of 2 for loops, for the output.

@authors:
	- Daniel Azevedo - A45827
	- Gonçalo Machuqueiro - A45831
	- Nuno Venancio - A45824

@git:
	https://bitbucket.org/lvsitanvs/pg/src/master/2%20Trabalho%20-%20Grupo%2012/

@since:
	02/11/2018
----------------------------------------------------------------------------- */

import java.util.Scanner;

public class Grid {

	public static void main (String[] args) {

		// main variables ------------------------------------------------------
		Scanner in = new Scanner(System.in);
		int rows = 0, cols = 0, hight = 0, width = 0;
		String row1 = "", row2 = "", row3 = "";

		// get user inputs -----------------------------------------------------
		while (rows < 2 || rows > 5) {
			System.out.print("Linhas (2..5)? ");
			rows = in.nextInt();
		}
		while (cols < 2 || cols > 8) {
			System.out.print("Colunas (2..8)? ");
			cols = in.nextInt();
		} 
		while (hight < 1 || hight > 8) {
			System.out.print("Altura (1..3)? ");
			hight = in.nextInt();
		}
		while (width < 1 || width > 5) {
			System.out.print("Largura (1..5)? ");
			width = in.nextInt();
		}

		// build the 3 different row Strings -----------------------------------
		// type "0---0---0" used in top and bottom
		for (int i = 0; i < cols; i++) {
			row1 += "0";
			for (int j = 0; j < width; j++)
				row1 += "-";							
		}
		row1 += "0\n";

		// type "|  |  |" used inside the rows
		for (int i = 0; i < cols; i++) {
			row2 += "|";
			for (int j = 0; j < width; j++)
				row2 += " ";
		}
		row2 += "|\n";

		// type "|---+---|" used to separate inner rows
		for (int i = 0; i < cols; i++) {
			if (i == 0)
				row3 += "0";
			else
				row3 += "+";
			for (int j = 0; j < width; j++)
				row3 += "-";
		}
		row3 += "0\n";

		// print the output ----------------------------------------------------
		System.out.print(row1); // first row of type "0---0---0"
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < hight; j++)
				System.out.print(row2); // type "|   |   |"
			if (i == rows - 1) // check if is in the last rows
				System.out.print(row1); // YES -> type "0---0---0"
			else
			System.out.print(row3);  // NO -> type "0---+---0"
		}
	}
}

/* -----------------------------------------------------------------------------
Report full description

Used variables:
	* Scanner in - to get user inputs;
	* int rows - the number of rows;
	* int cols - the number of columns;
	* int hight - the rows hight;
	* int width - the columns width;
	* int i - used in the for loops;
	* int j - used in the nested for loops;
	* String row1 - row of the type "0---0---0";
	* String row2 - row of the type "|   |   |";
	* String row3 - row of the type "0---+---0";

Algorithm explanation:
	1 - declaration and assignment of the main variables:
			* Scanner in;
			* int rows (0);
			* int cols (0);
			* int hight (0);
			* int width (0); 
			* String row1 - ("");
			* String row2 - ("");
			* String row3 - ("");

	2 - get user inputs for rows, cols, hight and width, with in.nextInt(),
		through the use of while loops check if the rows number are between 2
		and 5, the cols number are between 2 and 8, the hight number are between
		1 and 8, and the width number are between 1 and 5; user numbers must
		respect the conditions to exit the while loops;
 	
 	3 - build 3 different kinds of row Strings:
 			* row1 - for the number of cols append a "0", then, inside this for 
 					 loop, another one for the width number append a "-", after 
 					 exiting the for loops, append another "0";
			* row2 - for the number of cols append a "|", then, inside this for 
 					 loop, another one for the width number append a " ", after 
 					 exiting the for loops, append another "|";
 			* row3 - for the number of cols append a "|", then, inside this for 
 					 loop, another one for the width number append a "+", after 
 					 exiting the for loops, append another "|";

 	4 - print the output:
 			* print the first type of row (row1);
 			* then, for the number of rows times the hight:
 				* print the second type of row (row2);
 			* steel inside the for loop with the number of rows but outside the
 			  one with the hight:
 			  	* check if the loop is in the last row (last interaction), in
 			  	  the affirmative case, prints the first type of row (row1) and
 			  	  terminates, if its not the case, prints the third type of row
 			  	  (row3);
----------------------------------------------------------------------------- */