/* -----------------------------------------------------------------------------
@organization:
	ISEL - PG - Grupo 12

@summary:
	Class Equals with one function - main(String args[]).
	Program that reads a sequence of integers values ​​and displays how many of 
	them are the same. The sequence is readed until an element thats not an 
	integer value. There is no maximum number of values ​​to read.

@notes:
	The program uses a while and a for loops. The while to get and validate
	user's inputs, and, the for, for counting the integers repetitions.

@authors:
	- Daniel Azevedo - A45827
	- Gonçalo Machuqueiro - A45831
	- Nuno Venancio - A45824

@git:
	https://bitbucket.org/lvsitanvs/pg/src/master/2%20Trabalho%20-%20Grupo%2012/

@since:
	02/11/2018
----------------------------------------------------------------------------- */

import java.util.Scanner;

public class Equals {

	public static void main (String[] args) {

		// main variables ------------------------------------------------------
		Scanner in = new Scanner(System.in);
		String inputs = "", uniqueNums = "", repeatedNums = "";
		int len = 0, countRepNums = 0;

		// get user input, append it to inputs and count the ints until a non int
		while (in.hasNextInt()) {
			inputs += in.next() + " ";
			len++;
		}

		// build array with only the integers, one by array index --------------
		String[] arr = inputs.split(" ");
		
		// count num repetitions -----------------------------------------------
		for (int i = 0; i < len; i++) {

			// int not in uniqueNums, put it there
			if (!uniqueNums.contains(arr[i]))
				uniqueNums += arr[i];

			// int already in uniqueNums
			else {

				// int not in repeatedNums, put it there
				if (!repeatedNums.contains(arr[i])) {
					repeatedNums+=arr[i] + " ";
					// increment countRepNums by 2 to account int already in uniqueNums
					countRepNums += 2;
				}
				else
					// int is repeated another time, increment only by one
					countRepNums++;
			}
		}

		// Output --------------------------------------------------------------
		System.out.println("Iguais: " + countRepNums);
	}
}

/* -----------------------------------------------------------------------------
Report full description

Used variables:
	* Scanner in - to get user inputs;
	* String inputs - where the user input goes;
	* String uniqueNums - builted in the for loop with unique nums;
	* String repeatedNums - builted in the for loop with numbers that are
							already in the uniqueNums String;
	* int len - to count user inputs until a non integer;
	* int countRepNums -to count the numbers repetitions;
	* int i - used in the for loops;
	* String[] arr - builted from the String inputs with the String method
					 String.split();

Algorithm explanation:
	1 - declaration and assignment of the main variables:
			* Scanner in;
			* String inputs - ("");
			* String uniqueNums - ("");
			* String repeatedNums - ("");
			* int len - (0);
			* int countRepNums - (0);

	2 - while loop with stopping condition: in.nextInt(); to get user input, 
		append it to inputs with in.hasNext() and count the integers to len;
 	
 	3 - build an String array, arr, from the String inputs with only the
 		integers, one by index, using the String method String.split() with the
 		" " as argument; 

 	4 - count the number of repeated integers using a for loop, for valid user
 		inputs (len):
 			* if the String uniqueNums does not contain (using String.contains()
 			  method), the one in the arr[i], append it there
 			* by the other hand, if the integer in arr[i] is in uniqueNums:
 				* check if is also in repeatedNums String, in the affirmative 
 				  case, increment by one countRepNums, else, append it to
 				  repeatedNums and increment countRepNums by two, to account the
 				  one already in the uniqueNums;

 	5 - print the output;
----------------------------------------------------------------------------- */