/* -----------------------------------------------------------------------------
@organization:
	ISEL - PG - Grupo 12

@summary:
	Class Payment with one function - main(String args[]).
	Program that simulates payments in a vending machine with limited number of
	coins. User as to pass, as an argument, the number of coins of any type
	before the start of the program.
	The program ask user for the price of the product and the payment done, then
	calculates the change, the amount of coins to give in the change and the
	amount of coins that remain on the machine.
	At last outputs the change and the amount of coins of the change.

@notes:
	Program in an infinite loop, breaks if user sets 0 for the price.

@authors:
	- Daniel Azevedo - A45827
	- Gonçalo Machuqueiro - A45831
	- Nuno Venancio - A45824

@git:
	https://bitbucket.org/lvsitanvs/pg/src/master/2%20Trabalho%20-%20Grupo%2012/

@since:
	04/11/2018
----------------------------------------------------------------------------- */

import java.util.Scanner;

public class Payment {

	public static void main(String[] args) {

		// MAIN VARIABLES ------------------------------------------------------
		Scanner in = new Scanner(System.in);
		int initialCoinsNumber = Integer.parseInt(args[0]);
		int price = -1, payment = -1, change = 0, totalCash = 0;
		int[] singleCoinValue = {200, 100, 50, 20, 10, 5, 2, 1}; // in cents
		String[] coinStr = {"2E", "1E", "50c", "20c", "10c", "5c", "2c", "1c"};
		int[] inBoxCoins = new int[8];
		for (int i = 0; i < singleCoinValue.length - 1; i++) {
			inBoxCoins[i] = singleCoinValue[i] * initialCoinsNumber;
			totalCash += inBoxCoins[i];
		}

		System.out.println("Maquina carregada com " + initialCoinsNumber + " moedas de cada tipo");


		// PROGRAM'S MAIN LOOP (break if price == 0) ---------------------------
		while (true) {

			// get user inputs and check break condition
			System.out.print("Preco (em euros)? ");
			price = (int) Math.round(in.nextDouble() * 100);
			if (price == 0)
				break;
			System.out.print("Quantia recebida (em euros)? ");
			payment = (int) Math.round(in.nextDouble() * 100);		
			
			// change calculation and declaration and assignment of string output
			change = payment - price;
			String output = "Demasia: (" + (double)change / 100 + ") = ";

			// check if theres sufficient money for change in the box
			if (change > totalCash)
				output += "Nao ha troco. Venda interrompida.";

			// check if theres no need for change
			else if (change == 0)
				output += "Quantia certa";

			// check if payment is less than price
			else if (change < 0)
				output += "Quantia insuficiente";

			// do the change
			else {
				totalCash -= change;  // update totalCash in the machine

				// for every coin in the machine
				for (int i = 0; i < singleCoinValue.length - 1; i++) {

					// check, beginning with higher coins if change can be made
					if ((change / singleCoinValue[i]) > 0 && inBoxCoins[i] > 0) {
						int coinsNumber = change / singleCoinValue[i];

						// check if it gives more coins than the machine has
						if (coinsNumber > (inBoxCoins[i]) / singleCoinValue[i])
							coinsNumber = inBoxCoins[i] / singleCoinValue[i];

						// remove the value from change and machine
						change = change - coinsNumber * singleCoinValue[i];
						inBoxCoins[i] = inBoxCoins[i] - coinsNumber * singleCoinValue[i];

						// add value to the output string
						output += "" + (coinsNumber) + " x " + coinStr[i] + " + ";
					}
				}
				// remove the last part of the output string (" + ")
				output = output.substring(0, output.length() - 3);
			}				

			// output			
			System.out.println(output);
		}
	}
}

/* -----------------------------------------------------------------------------
Report full description

Used variables:
	* Scanner in - to get user inputs;
	* int initialCoinsNumber - the initial number of coins of any kind, passed 
							   by the user as an argument to the program;
	* int price - the article price, give by the user;
	* int payment - quantity received in money by the user;
	* int change - the change to output (payment - price);
	* int totalCash - to keep track of the total cash in the machine;
	* int i - used in the for loops;
	* int coinsNumber - used in the last for loop to keep track of the number of
						coins of one kind to return to user;
	* String output - string used in the output with change and number of coins
	                  of any kind to preform that change;
	* int[] singleCoinValue - array with the value of a single coin of any kind
							  in cents;
	* int[] inBoxCoins - array with the total value of any kind of coin in cents;
	* String[] coinStr - array with the value of a single coin of any kind;

Algorithm explanation:
	1 - declaration and assignment of the main variables:
			* Scanner in;
			* int initialCoinsNumber (with arg given by user);
			* int price (-1);
			* int payment (-1);
			* int change (0); 
			* String[] coinStr ("2E", "1E", "50c", "20c", "10c", "5c", "2c", "1c");
			* int[] singleCoinValue (200, 100, 50, 20, 10, 5, 2, 1);
			* int[] inBoxCoins (a product of initialCoinsNumber by singleCoinValue);
			* int totalCash (the sum of values in inBoxCoins);
	
	2 - output the total number of coins of any kind

	4 - start an infinite loop for the remaining of the program that will break 
		if user set price to 0 (while(true));

	5 - get user inputs for price and payment, round it and assign the values to the 
		corresponding variables, and also check if user inputed 0 for price to 
		break the while loop and terminate the program;
 	
 	6 - assignment of change (payment - price), and declaration and assignment
 		of the String output;

 	7 - verification of change conditions:
 			* change > totalCash (machine doesn't have coins to give change);
 			* change == 0 (machine doesn't need to give change);
 			* change < 0 (payment insufficient);
 				(all the tree conditions cancels the transaction, informs the
 				 user accordantly with output string and get user back to the 
 				 loop beginning);
 			* change < totalCash (machine will proceed to the next phase);

 	8 - update totalCash (totalCash - change);

 	9 - for every coin in the machine:
 			* check, beginning with higher coins if change can be made, with two
 			  conditions, (change / singleCoinValue) > 0 and inBoxCoins > 0, ie.
 			  if change divided by the coin value gives a number of coins bigger
 			  than zero, and, if there are these kind of coin in the box. If all
 			  is satisfied, initializes and assign the coinsNumber (int used to
 			  keep track of the coins of one kind to return to the user) and the
 			  program proceed deeper, if no, check the next kind of coin;
 			* check if the previous operation assigned to coinsNumber a value
 			  bigger than the number of available coins, if so, update 
 			  coinsNumber;
 			* remove the value (singleCoinValue x coinsNumber) from the change
 			  and inBoxCoins;
 			* finally, append to output the coinsNumber + "x" + coinStr + " + ";

 	10 - remove the trailing " + " from the output string using the substring 
 	    method

 	11 - print output;
 	----------------------------------------------------------------------------- */