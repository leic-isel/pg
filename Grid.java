/*
Faça um programa para escrever uma grelha com o aspeto apresentado no exemplo à
direita. O programa deve ler o número de linhas (NL) da grelha, o número de colunas (NC),
a altura de cada linha (AL) e a largura de cada coluna (LC), até serem introduzidos valores
dentro dos limites. Os limites mínimos e máximos de cada um dos valores são,
respetivamente: NL=(2..5), NC=(2..8), AL=(1..3), LC=(1..5).
*/

import java.util.Scanner;

public class Grid{
	
	public static void main(String[] args){
	
		Boolean capt = false;
		int numL=0, numC=0, altL=0, largC=0, aux=0;
		Scanner in = new Scanner(System.in);
		String auxStr = "O"; 
		String topbot = "";
		String mid = "";
		String midi ="";
		String inter ="";
		
		
		//Captura e valida os valores das variáveis a capturar
		
		while(!capt){
			System.out.print("Linhas (2..5)?:");
			numL = in.nextInt();
			System.out.println("");
			if (numL>1 && numL<6) capt=true;			
		}
		
		capt=false;
		
		while(!capt){
			System.out.print("Colunas (2..8)?");
			numC = in.nextInt();
			System.out.println("");
			if (numC>1 && numC<9) capt=true;			
		}
		
		capt=false;
		
		while(!capt){
			System.out.print("Altura (1..3)?");
			altL = in.nextInt();
			System.out.println("");
			if (altL>0 && altL<4) capt=true;			
		}
		
		capt=false;
		
		while(!capt){
			System.out.print("Largura (1..5)?");
			largC = in.nextInt();
			System.out.println("");
			if (largC>0 && largC<6) capt=true;			
		}
		
		//Desenha linha de topo/fundo
		
		while(aux<largC){
			auxStr = auxStr + "-";
			aux++;
		}
		for(int i=0; i<numC ; i++){
			topbot = topbot + auxStr;
		}
		
		aux=0;
		auxStr="|";
		//Desenha linha intermédia, com espaços.
		
		while(aux<largC){
			auxStr =auxStr + " ";
			aux++;
		}
		for(int i=0; i<numC ; i++){
			inter = inter + auxStr;
		}	
				
		aux=0;
		midi="O";
		auxStr="+";
		
		//Desenha a linha horizontal entre células internas
		
		while(aux<largC){
			midi = midi + "-";
			aux++;
		}
		
		aux=0;
		
		while(aux<largC){
			auxStr =auxStr + "-";
			aux++;
		}
		for(int i=0; i<(numC-1) ; i++){
			mid = mid + auxStr;
		}
		
		//Desenho Completo
		
		System.out.println(topbot+"O");
		for(int i=0; i<altL; i++) System.out.println(inter+"|");
		
		for(int i=0; i<(numL-1); i++){
			System.out.println(midi+mid+"O");
			for(int j=0; j<altL; j++) System.out.println(inter+"|");
		}	
		System.out.println(topbot+"O");
		
		}
}
