

/**
 * Created by nuno on 16-10-2017.
 */
public class Ficha1Questao1VB {
    public static void main(String[] args) {
        String str = "44509"; // String contendo o seu N.º aluno, ex: "44509"
        // System.out.println((int)'0'); // 48
        // System.out.println((int)'A'); // 65
        // System.out.println((int)'a'); // 97
        int i1 =  str.charAt(0) % 256 - (str.charAt(1) % 256) - (str.charAt(2) % 256)
                - (str.charAt(3) % 256) - (str.charAt(4) % 256);
        System.out.println("i1 = " + i1);
        System.out.println(str.charAt(3));
        System.out.println((int)str.charAt(3));
        System.out.println((int)(str.charAt(3) - '0' + 2));
        System.out.println(str.charAt(3) - '0');
        System.out.println("a" + str);
        System.out.println((char) ( ("a" + str).charAt(0) - 'a' + 'A'));
        // E nao se tivesse (char) antes?
        System.out.println(("a" + str).charAt(0) - 'a' + 'A');
    }
}
/*
i1 = -158
0
48
2
0
a44509
A
65
 */