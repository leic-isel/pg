
/**
 * Created by nuno on 16-10-2017.
 */
public class Ficha1Questao1VA {
    public static void main(String[] args) {
        String str = "43452"; // String contendo o seu N.º aluno, ex: "44509"
        // System.out.println((int)'0'); // 48
        // System.out.println((int)'A'); // 65
        // System.out.println((int)'a'); // 97
        int i1 =  str.charAt(0) % 256 + str.charAt(1) % 256 + str.charAt(2) % 256 + str.charAt(3) % 256 + str.charAt(4) % 256;
        System.out.println("i1 = " + i1);
        System.out.println(str.charAt(3));
        System.out.println((int)str.charAt(3));
        System.out.println((int)(str.charAt(3) - '0'));
        System.out.println(str.charAt(3) - '0');
        System.out.println("A" + str);
        System.out.println(("A" + str).charAt(0) - 'A' + 'a');
        // E se tivesse (char) antes?
        System.out.println((char) (("A" + str).charAt(0) - 'A' + 'a'));
    }
}
/*
i1 = 262
0
48
0
0
A44509
97
a
 */