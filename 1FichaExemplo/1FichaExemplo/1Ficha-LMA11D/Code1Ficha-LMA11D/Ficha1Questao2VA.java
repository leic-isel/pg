
import java.util.Scanner;

/**
 * Created by nuno on 16-10-2017.
 */
public class Ficha1Questao2VA {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Introduza quatro numeros:");
        double i1 = in.nextDouble();
        double i2 = in.nextDouble();
        double i3 = in.nextDouble();
        double i4 = in.nextDouble();
        // Calculo da media
        double mean = (i1 + i2 + i3 + i4) / 4.0;
        // Calculo do desvio padrao
        double std = Math.sqrt((Math.pow(i1 - mean, 2)
                            + Math.pow(i2 - mean, 2)
                            + Math.pow(i3 - mean, 2)
                            + Math.pow(i4 - mean, 2)) / 4.0);

        System.out.println("Media = " + mean);
        System.out.println("DP = " + std);

        // Imprimir numeros que se afastam mais do que 1 * desvio padrao da media
        double d1 = (i1 - mean);
        if (d1 <= 0)
            d1 = -d1;

        double d2 = (i2 - mean);
        if (d2 <= 0)
            d2 = -d2;

        double d3 = (i3 - mean);
        if (d3 <= 0)
            d3 = -d3;

        double d4 = (i4 - mean);
        if (d4 <= 0)
            d4 = -d4;

        int count = 0;
        boolean b1 = false;
        boolean b2 = false;
        boolean b3 = false;
        boolean b4 = false;

        if (d1 > std) {
            b1 = true;
            ++count;
        }

        if (d2 > std) {
            b2 = true;
            ++count;
        }

        if (d3 > std) {
            b3 = true;
            ++count;
        }

        if (d4 > std) {
            b4 = true;
            ++count;
        }

        if (count > 0) {
            System.out.println(count + " numeros afastam-se da media de um factor igual a DP");
            if (b1)
                System.out.println("i1 = " + i1);
            if (b2)
                System.out.println("i2 = " + i2);
            if (b3)
                System.out.println("i3 = " + i3);
            if (b4)
                System.out.println("i4 = " + i4);
        }

    }
}
// Ex1
// 6 2 3 1

// Ex2
// -10 1 2 10

// Ex3
// 4,2 4 3,6 3,7

