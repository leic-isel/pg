
import java.util.Scanner;

/**
 * Created by nuno on 16-10-2017.
 */
public class Ficha1Questao2VB {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Introduza quatro numeros:");
        double i1 = in.nextDouble();
        double i2 = in.nextDouble();
        double i3 = in.nextDouble();
        double i4 = in.nextDouble();
        // Calculo da media
        double mean = (i1 + i2 + i3 + i4) / 4.0;

        System.out.println("Media = " + mean);

        // Imprimir o numero que se afasta mais da media
        double d1 = (i1 - mean);
        if (d1 <= 0)
            d1 = -d1;

        double d2 = (i2 - mean);
        if (d2 <= 0)
            d2 = -d2;

        double d3 = (i3 - mean);
        if (d3 <= 0)
            d3 = -d3;

        double d4 = (i4 - mean);
        if (d4 <= 0)
            d4 = -d4;

        double greatestDif = d1;
        double greatest = i1;

        if (d2 > greatestDif) {
            greatestDif = d2;
            greatest = i2;
        }

        if (d3 > greatestDif) {
            greatestDif = d3;
            greatest = i3;
        }

        if (d4 > greatestDif) {
            greatestDif = d4;
            greatest = i4;
        }

        System.out.println("O numero que mais se afasta mais da media e' " + greatest);

        // NOTA: A presente resolução só considera o caso mais simples em que é apresentado
        // um único número mais afastado da média.
        // Para mostrar os vários números tinha que se fazer:
        if (greatestDif == d1)
            System.out.println(i1);
        if (greatestDif == d2)
            System.out.println(i2);
        if (greatestDif == d3)
            System.out.println(i3);
        if (greatestDif == d4)
            System.out.println(i4);

    }
}
// Ex2
// 6 2 3 1

// Ex2
// -10 1 2 10

// Ex2
// 4,2 4 3,6 3,7

// Ex2 (media = 0)
// -10 0 0 10
