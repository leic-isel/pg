
import java.util.Scanner;

/**
 * Created by nuno on 16-10-2017.
 */
public class Ficha1Questao2VB {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Introduza quatro numeros:");
        double i1 = in.nextDouble();
        double i2 = in.nextDouble();
        double i3 = in.nextDouble();
        double i4 = in.nextDouble();

        double d1 = (i1 - i2);
        if (d1 <= 0)
            d1 = -d1;

        double d2 = (i1 - i3);
        if (d2 <= 0)
            d2 = -d2;

        double d3 = (i1 - i4);
        if (d3 <= 0)
            d3 = -d3;

        double d4 = (i2 - i3);
        if (d4 <= 0)
            d4 = -d4;

        double d5 = (i2 - i4);
        if (d5 <= 0)
            d5 = -d5;

        double d6 = (i3 - i4);
        if (d6 <= 0)
            d6 = -d6;

//        System.out.println("d1 = " + d1);
//        System.out.println("d2 = " + d2);
//        System.out.println("d3 = " + d3);
//        System.out.println("d4 = " + d4);
//        System.out.println("d5 = " + d5);
//        System.out.println("d6 = " + d6);

        // Inicialmente, assume-se d1 como a maior diferenca
        double greatestDif = d1;
        double g1, g2;
        g1 = i1;
        g2 = i2;

        if (d2 > greatestDif) {
            greatestDif = d2;
            g1 = i1;
            g2 = i3;
        }
        if (d3 > greatestDif) {
            greatestDif = d3;
            g1 = i1;
            g2 = i4;
        }
        if (d4 > greatestDif) {
            greatestDif = d4;
            g1 = i2;
            g2 = i3;
        }
        if (d5 > greatestDif) {
            greatestDif = d5;
            g1 = i2;
            g2 = i4;
        }
        if (d6 > greatestDif) {
            greatestDif = d6;
            g1 = i3;
            g2 = i4;
        }

        System.out.println("Os dois numeros mais afastados sao " + g1 + " e " + g2);

    }
}
// Ex1
// 6 2 3 1

// Ex1
// 2 6 3 1

// Ex3
// -10 1 2 10

// Ex4
// 3,6 4,2 3,7 4,4

