
import java.util.Scanner;

/**
 * Created by nuno on 16-10-2017.
 */
public class Ficha1Questao2VA {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Introduza quatro numeros:");
        double i1 = in.nextDouble();
        double i2 = in.nextDouble();
        double i3 = in.nextDouble();
        double i4 = in.nextDouble();

        // Imprimir os dois maiores numeros
        double greatest1 = i1;
        double greatest2 = i2;

        if (i2 > greatest1) {
            greatest1 = i2;
            greatest2 = i1;
        }
        if (i3 > greatest1) {
            greatest2 = greatest1;
            greatest1 = i3;
        }
        else
            if (i3 > greatest2) {
                greatest2 = i3;
            }

        if (i4 > greatest1) {
            greatest2 = greatest1;
            greatest1 = i4;
        }
        else
            if (i4 > greatest2) {
                greatest2 = i4;
            }


        System.out.println("O maior numero e' " + greatest1);
        System.out.println("O segundo maior numero e' " + greatest2);


    }
}
// Ex1
// 6 2 3 1

// Ex1
// 2 6 3 1

// Ex3
// -10 1 2 10

// Ex4
// 3,6 4,2 3,7 4,4

