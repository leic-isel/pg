import java.util.Scanner;

public class Ficha_Grupo_2 {

	public static void main(String[] args) {

		double n2, n3, n4, big1, big2;
		Scanner in = new Scanner(System.in);

		System.out.println("Introduza quatro numeros:");

		big1 = in.nextDouble();
		big2 = big1;

		n2 = in.nextDouble();
		if (n2 > big1) big1 = n2;
		else big2 = n2;

		n3 = in.nextDouble();
		if (n3 > big1) {
			big2 = big1;
			big1 = n3;
		}
		else if (n3 > big2) big2 = n3;

		n4 = in.nextDouble();
		if (n4 > big1) {
			big2 = big1;
			big1 = n4;
		}
		else if (n4 > big2) big2 = n4;

		System.out.println("O maior numero e' " + big1);
		System.out.println("O segundo maior numero e' " + big2);
	}
}