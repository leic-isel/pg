/*
3. (Dice)
Fazer a simulação do lançamento de 2 dados num jogo de tabuleiro gerando valores
pseudoaleatórios com o método random() da classe Math. Só é mostrado o segundo
dado lançado após o utilizador premir a tecla "enter". No final é mostrada a
soma dos dados, que deverá ser dobrada se os dois valores forem iguais.

Output de exemplo:
C:\PG>java Dice
Primeiro dado = [1]
Prima enter
Segundo dado = [6]
Total = 7

C:\PG>java Dice
Primeiro dado = [3]
Prima entre
Segundo dado = [3]
Total a dobrar = 12
*/

import java.util.Scanner;

public class Dice {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		int MAX = 6; // maximun number on dice face
		int MIN = 1; // minimum number on dice face

		/* because Math.random() returns a number between 0 and 1.9999....
		we need first cast the double to int, to get rid of deciamal part, then
		to multiply the number returned by the max number we want, minus the
		minimum number we want, plus 1 and after all that, plus 1 another time*/
		int dice1 = (int) (Math.random() * ((MAX - MIN) + 1)) + MIN;
		System.out.print("Primeiro dado = [" + dice1 + "]");

		// to make user press [ENTER]
		in.nextLine();

		int dice2 = (int) (Math.random() * ((MAX - MIN) + 1)) + MIN;
		System.out.println("Segundo dado = [" + dice2 + "]");

		// Print the sum. If the two dice faces are equal, double the points
		if (dice1 == dice2)
			System.out.println("Total a dobrar = " + (dice1 + dice2) * 2);
		else {
			int total = dice1 + dice2;
			System.out.print("Total = " + total);
		}
	}
}