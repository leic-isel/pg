import java.util.Scanner;

public class Travel {
	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		int h1, h2, m1, m2, s1, s2, difH, difM, difS;

		// Get first time from user
		System.out.print("Partida? ");
		h1 = in.nextInt();
		m1 = in.nextInt();
		s1 = in.nextInt();

		// Get second time from user
		System.out.print("Chegada? ");
		h2 = in.nextInt();
		m2 = in.nextInt();
		s2 = in.nextInt();		
					
		// seconds calculation
		if (s2 < s1) {
			m1++;
			difS = ((s2 - s1) % 60) + 60;
		}
		else difS = s2 - s1;
			
		// minutes calculation
		if (m2 < m1) {
			h1++;
			difM = ((m2 - m1) % 60) + 60;
		}
		else difM = m2 - m1;
			
		// hours calculation
		difH = h2 - h1;

			
		// If departure is bigger than arrive print error message else print
		// duration
		if (h1 > h2) System.out.println("Partida > Chegada");
		else System.out.println("Duracao = " + difH + ":" + difM + ":" + difS); 
	}
}