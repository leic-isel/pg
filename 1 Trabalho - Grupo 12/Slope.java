/*
2. (Slope)
Escrever a equação reduzida da reta (y = mx + b) que passa por dois pontos
(x1, y1) e (x2, y2) introduzidos pelo utilizador. Na equação, m é o declive
(slope) da reta obtido pela expressão m = (y1 - y2) / (x1 - x2) e b corresponde
à ordenada na origem dada por b = y1 - m x1 ou b = y2 - m x2. Quando o declive
é infinito a equação será simplesmente (x = x1)

Output:
C:\PG>java Slope
(x1 y1)? 1 5.9
(x2 y2)? 0 3.2
y = 2.7 x + 3.2

C:\PG>java Slope
(x1 y1)? 3 6.9
(x2 y2)? 3 10
x = 3.0
*/

import java.util.Scanner;

public class Slope {

	public static void main(String[] args) {

		// Declaration of used variables
		Scanner in = new Scanner(System.in);
		double x1, y1, x2, y2, b, m;

		// Assignement of user input to point 1 (x1, y1)
		System.out.print("(x1 y1)? ");
		x1 = in.nextDouble();
		y1 = in.nextDouble();

		// Assignement of user input to point 2 (x2, y2)
		System.out.print("(x2 y2)? ");
		x2 = in.nextDouble();
		y2 = in.nextDouble();

		// Slope and Y intercept (where line crosses the Y axis) calculations
		m = (y1 - y2) / (x1 - x2);  // slope
		b = y1 - m * x1;  // Y intercept

		// Check if slope is or not an infinity and print according
		if (x1 == x2)
			System.out.println("x = " + x1);
		else
			System.out.println("y = " + m + " x + " + b);
	}
}


