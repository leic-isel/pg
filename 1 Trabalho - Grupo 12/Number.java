/*
1.  (Number)
Escrever em milhares, centenas, dezenas e unidades um valor (entre 0 e 9999)
introduzido pelo utilizador. São valorizadas as soluções que minimizem o número
de variáveis e de chamadas a print() / println()

output:
C:\ISEL\PG>java Number
Valor (0..9999)? 2018
2018 = 2 milhar(es) + 0 centena(s) + 1 dezena(s) + 8 unidade(s).
*/

import java.util.Scanner;

public class Number {

	public static void main (String[] args) {

		Scanner in = new Scanner(System.in);

		System.out.print("Valor (0..9999)? ");
		int num = in.nextInt();

		String result = "";

		// build the result String
		result += num / 1000 + " milhar(es) + "; //get thousands
		result += num % 1000 / 100 + " centena(s) + "; //get cents
		result += num % 100 / 10 + " dezena(s) + "; //get tens
		result += num % 10 + " unidade(s)."; //get units
		
		System.out.println(num + " = " + result);
	}
}