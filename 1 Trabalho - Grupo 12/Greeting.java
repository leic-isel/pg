/*
6. (Greeting)
Perguntar a hora e o nome do utilizador e apresentar a mensagem de cumprimento
(Bom dia, Boa tarde ou Boa noite) conforme a hora. Considere que a hora
introduzida é um valor inteiro entre 0 e 23. Cado seja lida a palavra "auto" (ou
outra qualquer) em vez de um valor inteiro, o programa deve utilizar a hora
atual, obtida pela expressão LocalTime.now()getHour() usando a classe LocalTime
do package java.time
*/

import java.util.Scanner;
import java.time.LocalTime;

public class Greeting {
	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		String userHour, userName;
		int hour = -1;

		// Get user inputs and assign it to userHour and userName
		System.out.print("Hora (auto)? ");
		userHour = in.nextLine();		
		System.out.print("Nome? ");
		userName = in.nextLine();

		// assign the first or the first and the second string userHour character's
		// integer value to hour
		if (userHour.length() == 1)
			hour = userHour.charAt(0) - '0';
		else 
			hour = (userHour.charAt(0) - '0') * 10 + (userHour.charAt(1) - '0');

		// assign the actual time to hour, if hour as not a valid value
		if (hour < 0 || hour > 23)
			hour = LocalTime.now().getHour();

		// print the greeting based on the hour
		if (hour < 12)
			System.out.println("Bom dia " + userName);
		else if (hour < 19)
			System.out.println("Boa tarde " + userName);
		else
			System.out.println("Boa noite " + userName);
	}
} 