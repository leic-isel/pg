/*
4. (Count4)
Ler 4 valores inteiros e indicar quantos são iguais, quantos têm valor par e
quantos têm valor ímpar. São valorizadas as soluções que minimizem o número de
de decisões binárias (instruções if).

Exemplo Output
C:\>PG>java Count4
4 valores? 3 2 8 2
Iguais:2 Pares:3 Impares:1

C:\>PG>java Count4
4 valores? 30 5 5 30
Iguais:4 Pares:2 Impares:2

C:\>PG>java Count4
4 valores? 31 5 9 11
Iguais:0 Pares:0 Impares:4
*/

import java.util.Scanner;

public class Count4 {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		int n1, n2, n3, n4, even = 0, equals = 0;

		// Get user numbers
		System.out.print("4 valores? ");
		n1 = in.nextInt();
		n2 = in.nextInt();
		n3 = in.nextInt();
		n4 = in.nextInt();

		// Equals incrementation
		if (n1 == n2 || n1 == n3 || n1 == n4) { equals++; }
		if (n2 == n1 || n2 == n3 || n2 == n4) { equals++; }
		if (n3 == n1 || n3 == n2 || n3 == n4) { equals++; }
		if (n4 == n1 || n4 == n2 || n4 == n3) { equals++; }

		// Even numbers incrementation
		if (n1 % 2 == 0) { even++; }
		if (n2 % 2 == 0) { even++; }
		if (n3 % 2 == 0) { even++; }
		if (n4 % 2 == 0) { even++; }

		// Odd numbers are total number minus even numbers
		System.out.println("Iguais:" + equals + " Pares:" + even + " Impares:" + (4 - even));
	}
}
