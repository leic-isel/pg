
public class ArrayUtils {

    public static void replace (int [] arr, int val, int newVal) {

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == val) {
                arr[i] = newVal;
            }
        }
    }

    public static int[] findMaxMin (int[] arr) {
        int[] r = new int[2];
        r[0] = arr[0];
        r[1] = arr[0];

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > r[0])
                r[0] = arr[i];
            else if (arr[i] < r[1])
                r[1] = arr[i];
        }
        return r;
    }

    public static void main (String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9};

        replace(arr, 5, 10);
        printArray(arr);
        printArray(findMaxMin(arr));
    }

    public static void printArray(int[] arr) {
        System.out.print("[" + arr[0]);
        for (int i = 1; i < arr.length; i++) {
            System.out.print(" " + arr[i]);
        }
        System.out.print("]\n");
    }
}