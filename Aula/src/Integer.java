public class Integer {
    public static int parseInt(String number) {

        int num = 0;
        int multiplier = 1;
        for (int i = number.length() - 1; i >= 0; i--) {
            if (number.charAt(i) < 48 || number.charAt(i) > 57) {
                System.out.println("Nao e um numero");
                return num;
            }
            else {
                 num += (int)(number.charAt(i) - 48) * multiplier;
                 multiplier *= 10;
            }
        }
        return num;
    }
    public static void main(String[] args) {

        System.out.println(parseInt("2345"));
        System.out.println(parseInt("oi"));
    }

}
