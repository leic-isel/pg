public class MatrixUtils {

    public static int[][] createTriangle(int h) {
        int w = 2 * h - 1, start = 0, end = w - 1;
        int[][] arr = new int[h][w];

        for (int i = h - 1; i >= 0; i--) {
            for (int j = w - 1; j >= 0; j--) {
                if (j >= start && j <= end) {
                    arr[i][j] = 1;
                }
            }
            start++;
            end--;
        }
        return arr;
    }


    public static void printMatrix(int[][] matrix) {

        for (int i = 0; i < matrix.length; i++) {
            ArrayUtils.printArray(matrix[i]);
        }
    }


    public static void main (String[] args) {

        printMatrix(createTriangle(4));
        printMatrix(createTriangle(6));
        printMatrix(createTriangle(8));
    }
}
