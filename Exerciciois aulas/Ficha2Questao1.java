public class Ficha2Questao1 {
	
	public static void main(String[] args){
		int n = 45824;
		int[] digits = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		String s = "";
		for (int i = 0; n > 0; i++, n /= 10) {
			int r = n % 10;
			while (r < 5) {
				System.out.println("r << 1 = " + (r <<1)+ ", digit = " + digits[r <<1]);
				s = digits[r << 1] + s;
				r <<= 1;
			}
			s = "$" + s;
		}
		System.out.print(s);
 	}
}