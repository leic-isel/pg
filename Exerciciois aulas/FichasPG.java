import java.util.Scanner;

public class FichasPG {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("n? ");
		int n = in.nextInt();
		double n1 = 0;
		double n2 = 0;
		double n3 = 0;

		while (n > 0) {
			double num = in.nextDouble();
			if (num > n1) {
				n3 = n2;
				n2 = n1;
				n1 = num;
			}
			else if (num > n2) {
				n3 = n2;
				n2 = num;
			}
			else if (num > n3) n3 = num;
			n--;
		}

		System.out.println("Media das tres melhores: " + (n1 + n2 + n3) / 3);
	}
}