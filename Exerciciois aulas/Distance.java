import java.util.Scanner;

public class Distance {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		double x1, x2, y1, y2, distance;

		System.out.println("\nDistance calculator between two points\n");
		System.out.print("X1 value: ");
		x1 = in.nextDouble();
		System.out.print("Y1 value: ");
		y1 = in.nextDouble();
		System.out.print("X2 value: ");
		x2 = in.nextDouble();
		System.out.print("Y2 value: ");
		y2 = in.nextDouble();

		// Math.sqrt(number) - raiz quadrada de um número
		// Math.pow(base, power) - potência de um número
		distance = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));

		System.out.println("\nThe distance between points are: " + distance);
	}
}