import java.util.Scanner;

public class RotateArray {

	public static void main(String[] args) {

		// main variables ------------------------------------------------------
		Scanner in = new Scanner(System.in);
		String strArray = "", strShifted = "";

		// Get array dimension -------------------------------------------------		
		System.out.print("Array Dimension? ");
		int dim = in.nextInt();

		// Get array values ----------------------------------------------------
		int[] arr = new int[dim];
		System.out.println("Introduza " + dim + " valores: ");
		for(int i = 0; i < dim; i++) {
			arr[i] = in.nextInt();
			strArray += arr[i] + " ";
		}

		// Get number of positional array shifts -------------------------------
		System.out.print("Number of positional shifts: ");
		int shifts = in.nextInt();

		// Get shifts direction ------------------------------------------------
		System.out.print("Shift array to right? (y ou n): ");
		String choice = in.next();

		// Build new shifted array ---------------------------------------------
		int[] shifted = new int[dim];
		for(int i = 0; i < dim; i++) {

			// shift to left
			if(choice.equals("n")) {
				shifted[i] = arr[(i + shifts) % arr.length];
				strShifted += shifted[i] + " ";				
			}

			// shift to right
			else {
				shifted[i] = arr[((i - shifts) + arr.length) % arr.length];
				strShifted += shifted[i] + " ";
			}
		}

		// Show output to user -------------------------------------------------
		System.out.println("Original array: " + strArray);
		System.out.println("Shifted array: " + strShifted);
	}
}

