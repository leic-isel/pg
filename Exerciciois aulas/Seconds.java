/* Programa que lê tempo em segundos (na mesma linha)

Ex:
Input - 3727
Output - 3727 segundos são 1 hora(s), 2 minuto(s) e 7 segundo(s)
*/

import java.util.Scanner;

public class Seconds {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);

		System.out.print("Insira os segundos: ");

		int num  = in.nextInt();
		int hour = num / 60 / 60;
		int minute = num / 60 % 60;
		int second = num % 60;

		System.out.println(num + "segundos sao " + hour + " hora(s), " + minute + 
							" minuto(s) e " + second + " segundo(s)");
	}
}
