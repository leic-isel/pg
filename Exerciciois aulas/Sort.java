import java.util.Scanner;

public class Sort {

	public static void main(String[] args) {
	
		int max, middle, min, num2, num3;
		Scanner in = new Scanner(System.in);

		System.out.println("Introduza tres numeros: ");

		// first number
		max = in.nextInt();
		middle = max;
		min = max;

		// second number
		num2 = in.nextInt();
		if (num2 > max) max = num2;
		else if (num2 < min) min = num2;

		// thirst number
		num3 = in.nextInt();
		if (num3 > max) {
			middle = max;
			max = num3;
		}
		else if (num3 < min) {
			middle = min;
			min = num3;
		}
		else middle = num3;

		// Print sorted numbers
		System.out.println("" + min + ", " + middle + ", " + max);
	}
}