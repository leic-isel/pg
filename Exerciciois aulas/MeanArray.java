
import java.util.Scanner;

public class MeanArray {

	public static void main(String[] args) {

		// main variables ------------------------------------------------------
		Scanner in = new Scanner(System.in);
		int max, min; 
		double sum = 0.0;
		
		// Get user array dimension --------------------------------------------
		System.out.print("Dim? ");
		int dim = in.nextInt();
		
		// Build user array ----------------------------------------------------
		int[] arr = new int[dim];
		System.out.println("Introduza " + dim + " valores: ");
		for(int i = 0; i < dim; i++)
			arr[i] = in.nextInt();

		// Get min, max and sum -----------------------------------------------
		max = min = arr[0];
		for(int i = 0; i < dim; i++) {
			sum += arr[i];
			if(arr[i] > max) max = arr[i];
			else if(arr[i] < min) min = arr[i];
		}

		// Output results ------------------------------------------------------
		double mean = sum / arr.length;
		System.out.println("Max: " + max + " Min: " + min + " Mean: " + mean);
	} 
}

/* Solução do professor ########################################################

bolean first = True;
int len = in.nextInt();
int[] arr = new int[len];
int big, small, sum = 0;

for (int i = 0; i < len; ++i) {
	int n = in.nextInt();
	arr[i] = n;
	if (first) {
		big = small = n;
		first = False;
	}
	if (n > big) big = n;
	else if (n < small) small = n;
	sum += n; 
	}
}
double avg = (double) sum / len;
System.out.println(big, small, avg)

############################################################################# */

