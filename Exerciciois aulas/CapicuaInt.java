
import java.util.Scanner;

public class CapicuaInt {

	public static void main(String[] args) {

		// Class varriables ----------------------------------------------------
		Scanner in = new Scanner(System.in); // For user input
		System.out.print("Numero: ");
		int num = in.nextInt();	// Get the number from user
		int invertedNum = 0; // Empty number to be construct in loop calculations
		int numLen = (int) Math.log10(num) + 1; // Length of user number

		/* Build invertedNum ---------------------------------------------------
		n - used in the boolean condition, when n == numLen, stops loop
		m - used to exponentiate the number get in the remainder of int division
		divider - used to get units, tens, cents, etc
		remainder - used to get the integer division's remainder */
		for (
			// variables initialization
			int n = 0, m = numLen -1, divider = 1, remainder = 10;

			// boolean condition to exit loop
			 n < numLen;

			 // variables incrementation / decrementation
			 n++, m--, divider *= 10, remainder *= 10
			 )

			// ivertedNum build
			invertedNum += (num % remainder / divider * Math.pow(10, m));
			
		// Inform user if the number introdutced is or not "Capicua" -----------
		if (num == invertedNum) System.out.println(num + " e capicua");
		else System.out.println(num + " nao e capicua");
	}
}

/* Código do professor ---------------------------------------------------------

import java.util.Scanner;

public class CapicuaInt {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in)
		int n = in.nextInt()
		int copy = n, rem, dig, c = 0, inv = 0;

		while (copy > 0) {
			dig = copy % 10;
			copy /= 10;
			inv = inv * 10 + dig;
		}

		if (num == invertedNum) System.out.println(num + " e capicua");
		else System.out.println(num + " nao e capicua");
	}
} ----------------------------------------------------------------------------*/