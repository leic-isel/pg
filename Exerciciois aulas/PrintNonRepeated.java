import java.util.Scanner;

public class PrintNonRepeated {

	public static void main(String[] args) {

		// main variables ------------------------------------------------------
		Scanner in = new Scanner(System.in);

		// Get array dimension -------------------------------------------------		
		System.out.print("Array Dimension? ");
		int dim = in.nextInt();

		// Get array values ----------------------------------------------------
		int[] arr = new int[dim];		 
		System.out.println("Introduza " + dim + " valores: ");
		for(int i = 0; i < dim; i++)
			arr[i] = in.nextInt();

		// Output --------------------------------------------------------------
		String toPrint = "";
		for (int i = 0; i < dim; i++)
			if (!toPrint.contains(Integer.toString(arr[i])))
				toPrint += arr[i] + " ";
		System.out.println(toPrint);
		
		/* Second Implementation ###############################################
		// Output --------------------------------------------------------------
		for (int i = 0; i < dim; i++) { // Pick all array elements
			int j;
			for (j = 0; j < i; j++) // Check if element is already printed
				if (arr[i] == arr[j]) 
					break;
			if (i == j) // If not printed earlier, then print it
				System.out.print(arr[i] + " ");
		}
		##################################################################### */

		/* First Implementation ################################################
		// Sort the array ------------------------------------------------------
		for (int i = 0; i < dim; i++) {
			for (int j = 0; j < dim - i - 1; j++){
				int aux;
				if (arr[j] > arr[j + 1]) {
					aux = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = aux; 
				}
			}
		}

		// Print unique numbers on the array -----------------------------------
		System.out.print(arr[0]);
		int printed = arr[0];
		for (int i = 1; i < dim; i++) {
			if (arr[i] != printed) {
				System.out.print(" " + arr[i]);
				printed = arr[i];
			}

		}
		####################################################################### */
	}
}
