import java.util.Scanner;

public class StandardDeviation {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Introduza quatro numeros:");
        double i1 = in.nextDouble();
        double i2 = in.nextDouble();
        double i3 = in.nextDouble();
        double i4 = in.nextDouble();

        // Mean
        double mean = (i1 + i2 + i3 + i4) / 4.0;
        // Standard Deviation
        double std = Math.sqrt((Math.pow(i1 - mean, 2)
        	 + Math.pow(i2 - mean, 2) + Math.pow(i3 - mean, 2)
        	 + Math.pow(i4 - mean, 2)) / 4.0);

        System.out.println("Media = " + mean);
        System.out.println("DP = " + std);

        // Check if there are numbers bigger than the Standard Deviation
        int biggerThenStd = 0;
        boolean i1Bigger = false;
        boolean i2Bigger = false;
        boolean i3Bigger = false;
        boolean i4Bigger = false;

        if (i1 - mean > std || -i1 + mean > std) {
        	i1Bigger = true;
        	biggerThenStd++;
        } 
        if (i2 - mean > std || -i2 + mean > std) {
        	i2Bigger = true;
        	biggerThenStd++;
        }
        if (i3 - mean > std || -i3 + mean > std) {
        	i3Bigger = true;
        	biggerThenStd++;
        }
        if (i4 - mean > std || -i4 + mean > std) {
        	i4Bigger = true;
        	biggerThenStd++;
        } 

        // Print out numbers bigger then standard deviation if they exist
        if (biggerThenStd > 0) {
        	String msg = " numeros afastam-se da media de um factor igual a DP";
        	System.out.println(biggerThenStd + msg);
        	if (i1Bigger) System.out.println("i1 = " + i1);
        	if (i2Bigger) System.out.println("i2 = " + i2);
        	if (i3Bigger) System.out.println("i3 = " + i3);
        	if (i4Bigger) System.out.println("i4 = " + i4);
        }
    }
}
