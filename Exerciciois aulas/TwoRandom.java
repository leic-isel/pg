
public class TwoRandom {

	public static void main(String[] args) {

		int MAX = 5; // maximun number
		int MIN = 0; // minimum number		
		int counter = 0;
		int num1, num2;

		do {
			counter++;
			num1 = (int) (Math.random() * ((MAX - MIN) + 1)) + MIN;
			num2 = (int) (Math.random() * ((MAX - MIN) + 1)) + MIN;
			System.out.println(num1);
			System.out.println(num2);
			if (num1 == num2)
				System.out.println("Iguais");
		}
		while (num1 == num2);

		System.out.println("OK, tentativas = " + counter);
	}
}
