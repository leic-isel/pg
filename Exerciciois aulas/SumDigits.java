import java.util.Scanner;

public class SumDigits {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);

		System.out.println("Introduza um numero de 3 algarismos e clique 'ENTER': ");

		int num = in.nextInt();
		int unit = num % 10;
		int tens = num / 10 % 10;
		int hundrs = num / 100;

		int sum = unit + tens + hundrs;
		
		System.out.println("O resultado da soma dos 3 algarismos de " + num + ": " + sum);

	}
} 