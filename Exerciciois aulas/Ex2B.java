/*
$java Ex2 
Introduza quatro numeros: 2 6 3 1 
Os dois numeros mais afastados sao 6.0 e 1.0
*/

import java.util.Scanner;

public class Ex2B {

	public static void main (String[] args) {

		Scanner in = new Scanner(System.in);
		Double n1, n2, n3, n4, higher, lower;

		System.out.print("Introduza quatro numeros: ");

		n1 = in.nextDouble();
		higher = n1;
		lower = n1;

		n2 = in.nextDouble();
		if (n2 > higher) higher = n2;
		else if (n2 < lower) lower = n2;
		
		n3 = in.nextDouble();
		if (n3 > higher) higher = n3;
		else if (n3 < lower) lower = n3;

		n4 = in.nextDouble();
		if (n4 > higher) higher = n4;
		else if (n4 < lower) lower = n4;

		System.out.println("Os dois numeros mais afastados sao " + higher + " e " + lower);

	}
}