import java.util.Scanner;

public class tenNums {
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Escreva 10 numeros: ");
		
		double mean = in.nextDouble();
		double min = mean;
		double max = mean;
		double total = 9;

		while (total > 0) {
			double num = in.nextDouble();
			mean += num;
			if (num > max) max = num;
			else if (num < min) min = num;
			total--;
		}
		System.out.println("Maior: " + max);
		System.out.println("Menor: " + min);
		System.out.println("Media: " + mean / 10);
	}
}