/* Programa para criar array com dimensão dada pelo utilizador
e preencher com valores indicados pelo utilizador

FillArray
*/

import java.util.Scanner;

public class FillArray {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		System.out.print("Dim? ");
		int dim = in.nextInt();

		int[] arr = new int[dim];

		System.out.println("Introduza " + dim + " valores: ");

		for(int i = 0; i < dim; i++) {
			arr[i] = in.nextInt();
		}

		System.out.println("Array:");
		for (int i = 0; i < dim; i++) {
			System.out.println(arr[i]);
		}
	} 
}
