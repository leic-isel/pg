public class PGLI12D_Ficha2_vB_1 {

	public static void main(String[] args) {

		int n = 45824;
		int[] digits = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		String s = "";
		for (int i = 0; n > 0; n /= 10, i++) {
			int r = n % 10;
			for(; r > 0;) {
				s = s + digits[--r];
			}
			s = s + "#";
		}
		System.out.println(s);
		for (int i = 0; i < s.length(); ++i) {
			if (s.charAt(i) == '#') {
				System.out.println();
				for (int j = 0; j < i; ++j)
					System.out.print(' ');
			}
			else
				System.out.print(s.charAt(i));
		}
	}
}