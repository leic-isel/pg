import java.util.Scanner;

public class TimeToSec {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		int hours = -1;
		int minuts = -1;
		int seconds = -1;
		int total = -1;
		String msg1 = "têm de ser entre 0 e 59. Corra o programa novamente!";
		String msg2 = "têm de ser entre 0 e 23. Corra o programa novamente!";

		System.out.print("Introduza o numero de horas (0 - 23): ");
		hours = in.nextInt();

		if(hours >= 0 && hours < 24) {
			System.out.print("Introduza o numero de minutos (0 - 59): ");
			minuts = in.nextInt();

			if (minuts >= 0 && minuts < 60) {
				System.out.print("Introduza o numero de segundos (0 - 59): ");
				seconds = in.nextInt();					
					
				if (seconds >= 0 && seconds < 60) {}
				else {
					System.out.println("Os segundos " + msg1);
					System.exit(0);
				}
			}
			else {
				System.out.println("Os minutos " + msg1);
				System.exit(0);
			}			
		}
		else {
			System.out.println("As horas " + msg2);
			System.exit(0);
		}		

		total = hours * 3600 + minuts * 60 + seconds;
		System.out.println("\n" + hours + " horas + " + minuts + " minutos + " +
						   seconds +  " segundos, sao: " + total + " segundos");
	}
}