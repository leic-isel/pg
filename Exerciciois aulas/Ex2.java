/*
$java Ex2 
Introduza quatro numeros: 6 2 3 1 
O maior numero e’ 6.0 
O segundo maior numero e’ 3.0

*/
import java.util.Scanner;

public class Ex2 {

	public static void main (String[] args) {

		Scanner in = new Scanner(System.in);
		Double n1, n2, n3, n4, h1, h2;

		System.out.print("Introduza quatro numeros: ");
		n1 = in.nextDouble();
		h1 = n1;
		h2 = n1;

		n2 = in.nextDouble();
		if (n2 > h1) 
			h1 = n2;
		else 
			h2 = n2;

		n3 = in.nextDouble();
		if (n3 > h1) {
			h2 = h1;
			h1 = n3;
		}
		else if (n3 > h2) 
			h2 = n3;

		n4 = in.nextDouble();
		if (n4 > h1) {
			h2 = h1;
			h1 = n4;
		}
		else if (n4 > h2) 
			h2 = n4;

		System.out.println("O maior numero e' " + h1);
		System.out.println("O segundo maior numero e' " + h2);
	}
}