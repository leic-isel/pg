import java.util.Scanner;

public class GreaterThanFirst {
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Escreva 10 numeros: ");
		int num = in.nextInt();
		int total = 9;

		while (total > 0) {
			int num2 = in.nextInt();
			if (num2 > num) System.out.println(num2);
			total--;
		}
	}
}