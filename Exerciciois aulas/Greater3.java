import java.util.Scanner;

public class Greater3 {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);

		System.out.println("Introduza 3 números:");

		int x = in.nextInt();
		int y = in.nextInt();
		int z = in.nextInt();
		int m;

		if (x >= y) m = x;
		else m = y;
		if (z >= m) m = z;

		System.out.println("O numero maior: " + m);		
	}
}
