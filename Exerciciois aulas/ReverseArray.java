import java.util.Scanner;

public class ReverseArray {

	public static void main(String[] args) {

		// main variables ------------------------------------------------------
		Scanner in = new Scanner(System.in);

		// Get array dimension -------------------------------------------------		
		System.out.print("Array Dimension? ");
		int dim = in.nextInt();

		// Get array values ----------------------------------------------------
		int[] arr = new int[dim];		 
		System.out.println("Introduza " + dim + " valores: ");
		for(int i = 0; i < dim; i++) {
			arr[i] = in.nextInt();
		}

		// Build rotated array --------------------------------------------------
		int[] rotated = new int[dim];
		for (int i = 0; i < dim; i++) {
			rotated[i] = arr[arr.length - 1 - i];
		}

		// Output ---------------------------------------------------------------
		System.out.print("Original: ");
		System.out.print("[");
		if (arr.length > 0) {
			System.out.print(arr[0]);
			for (int i = 1; i < dim; i++)
				System.out.print(", " + arr[i]);
		}
		System.out.print("]\n");

		System.out.print("Reversed: ");
		System.out.print("[");
		if (rotated.length > 0) {
			System.out.print(rotated[0]);
			for (int i = 1; i < dim; i++)
				System.out.print(", " + rotated[i]);
		}
		System.out.print("]\n");
	}
}