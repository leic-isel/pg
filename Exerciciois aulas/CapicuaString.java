/* Programa que le uma string (in.next()) e verifica se é uma
capicua (ou palindrome) lê-se da mesma forma da direita para
a esquerda e vice-versa
*/

import java.util.Scanner;

public class CapicuaString {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		String str = in.next();
		int lenghtStr = str.length();
		int n = 0;
		boolean isCapicua = true;


		while (n < lenghtStr / 2 && isCapicua) {
			if (str.charAt(lenghtStr - n - 1) == str.charAt(n)) n++;
			else isCapicua = false;

		}
		if (isCapicua) System.out.println(str + " e capicua");
		else System.out.println(str + " nao e capicua");
	}
}